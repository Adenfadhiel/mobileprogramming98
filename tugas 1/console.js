//if else
var nama = ""
var peran = ""

if(nama == ""){
    console.log("Nama harus diisi!");
} else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else{
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if(peran == "penyihir"){
	    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(peran == "guard"){
	    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "werewolf"){
	    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else{
	    console.log("Peran tidak ada, tidak menggunakan huruf kapital");
    }
}

var nama = "John"
var peran = ""

if(nama == ""){
    console.log("Nama harus diisi!");
} else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else{
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if(peran == "penyihir"){
	    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(peran == "guard"){
	    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "werewolf"){
	    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else{
	    console.log("Peran tidak ada, tidak menggunakan huruf kapital");
    }
}

var nama = "Jane"
var peran = "penyihir"

if(nama == ""){
    console.log("Nama harus diisi!");
} else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else{
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if(peran == "penyihir"){
	    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(peran == "guard"){
	    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "werewolf"){
	    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else{
	    console.log("Peran tidak ada, tidak menggunakan huruf kapital");
    }
}

var nama = "Jenita"
var peran = "guard"

if(nama == ""){
    console.log("Nama harus diisi!");
} else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else{
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if(peran == "penyihir"){
	    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(peran == "guard"){
	    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "werewolf"){
	    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else{
	    console.log("Peran tidak ada, tidak menggunakan huruf kapital");
    }
}

var nama = "Junaedi"
var peran = "werewolf"

if(nama == ""){
    console.log("Nama harus diisi!");
} else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else{
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if(peran == "penyihir"){
	    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if(peran == "guard"){
	    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "werewolf"){
	    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else{
	    console.log("Peran tidak ada, tidak menggunakan huruf kapital");
    }
}

//switch case
var hari = 21 ;
var bulan = 1 ;
var tahun = 1945 ;
  
switch (bulan) 
{
case 1:
       console.log( bulan = " Januari  ");
       break;
case 2:
       console.log( bulan = " Februari ");
       break;
case 3:
       console.log( bulan = " Maret ");
       break; 
case 4:
       console.log( bulan = " April ");
       break;
case 5:
       console.log( bulan = " Mei ");
       break;
case 6:
       console.log( bulan = " Juni ");
       break;
case 7:
       console.log( bulan = " Juli ");
       break;
case 8:
       console.log( bulan = " Agustus ");
       break;
case 9:
       console.log( bulan = " September ");
       break;
case 10:
       console.log( bulan = " Oktober ");
       break;
case 11:
       console.log( bulan = " November ");
       break;
case 12:
       console.log( bulan = " Desember ");
       break;
}
